"""
Initial D3 Card SANWA v1.0
Programmed by: winteriscoming
Special thanks to: Metallic

edited by: SaturnNiGHTS
refactored by : LayeredRx

Serial interface referenced from jpnevulator.py script.

jpnevulator info:

jpnevulator.py version 2.1.3
Copyright (C) 2015 Philipp Klaus
This is free software.  You may redistribute copies of it under the terms of
the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.
There is NO WARRANTY, to the extent permitted by law.
"""


import argparse
import os
import sys
import importlib

supported_games = []
executing_emulation = None

def start_emulation(request, comport):
    global executing_emulation
    if executing_emulation != None:
        executing_emulation.StopRequested = True
    selected_game = None
    for game in supported_games:
        if game.lower() == args.game.lower():
            print("Loading support for %s" % game)
            selected_game = game
            break
    if selected_game == None:
        print("Unsupported game '%s' expected one of : \n\t%s" % (request, "\n\t".join(supported_games)))
        sys.exit(-1)

    game_package = importlib.import_module("%s.CardReader"%game)
    executing_emulation = game_package.CardReader(comport)


#if there is a match, load the CardReader from that Directory and continue

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-cp', '--comport', metavar='COMPORT', help="The serial device. Example: -t COM1")
    parser.add_argument('-g', '--game', required=True, help='Name of game to load support files for. Example: -g ID3')
    args = parser.parse_args()

    main_directory = os.path.abspath(os.path.dirname(__file__))
    game_support_directory = "%s%sGameSupport" % (main_directory, os.sep)
    _, supported_games, _ = os.walk(game_support_directory).__next__()
    sys.path.append(game_support_directory)
    start_emulation(args.game, args.comport)
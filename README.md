# NaomiCardReaderEmulator

Initial D3 Card SANWA v1.0
Programmed by: winteriscoming
Special thanks to: Metallic

edited by: SaturnNiGHTS

refactored by : LayeredRx

Serial interface referenced from jpnevulator.py script.

jpnevulator info:

jpnevulator.py version 2.1.3
Copyright (C) 2015 Philipp Klaus
This is free software.  You may redistribute copies of it under the terms of
the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.
There is NO WARRANTY, to the extent permitted by law.


This emulator is designed to emulate the card reader found in Sega Arcade games
such as Initial D 3

Individual game support should be added to the GameSupport directory and new
games can be created using the files found in the template sub directory.



pip install pyserial

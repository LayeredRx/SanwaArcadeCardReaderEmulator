



class CardBase():
    """CardBase
    This is the base class to be used for card objects implemented for the supported games"""
    def __init__(self, cardName, typeName):
        self.cardName = cardName #unique name of card
        self.typeName = typeName #name of game type card belongs to
        self.printData = [] #data printed to card
        self.gameData = [] #game data saved to card
    
    def __str__(self):
        return "%s card : %s" % (self.typeName, self.cardName)
    
    def getGameData(self):
        pass
    
    def getPrintData(self):
        pass
    
    def setGameData(self, data):
        pass
    
    def setPrintData(self, data):
        pass
        
    def setCardInserted(self):
        pass
    
    def setCardEjected(self):
        pass
    
    def writeToFile(self, filename):
        pass
        
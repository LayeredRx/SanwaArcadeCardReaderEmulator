from enum import Enum
##
## General values
##
PACKET_PREAMBLE = 0x02
END_OF_COMMAND  = 0x03


##
## Card reader states (retaining leading zeros for easier debugging later)
##
CARDSLOT_OPEN      = 0b10000000
CARDSLOT_CLOSED    = 0b01000000
DISPENCER_FULL     = 0b00100000 #wont ever be empty
CARDSLOT_OCCUPIED  = 0b00011000
CARDSLOT_EMPTY     = 0b00000000
CARDSLOT_DISPENCE  = 0b00000100 #in question still
CARD_AT_PRINTER    = 0b00000111


##
## Results
##
class STATUS(Enum):
    OK  = 0x30
    ACK = 0x06


##
## Result1
##
class CARD_STATE(Enum):
    NO_CARD_IN_READER = 0x30 # 00110000
    CARD_PRESENT      = 0x31 # 00110001
    CARD_BAD_STATUS   = 0x32 # 00110010
    TWO_CARDS         = 0x33 # 00110011
    CARD_IN_SLOT      = 0x34 # 00110100

##
## Result2
##
class ERROR_STATE(Enum):
    SUCCESSFUL     = 0x30
    READ_ERROR     = 0x31
    WRITE_ERROR    = 0x32
    CARD_JAM       = 0x33
    MOTOR_ERROR    = 0x34
    PRINTER_ERROR  = 0x35
    ILLEGAL_OP     = 0x38
    VOLTAGE_ERROR  = 0x40
    WRITER_ERROR   = 0x41
    TRACK_1_ERROR  = 0x51
    TRACK_2_ERROR  = 0x52
    TRACK_3_ERROR  = 0x53
    TRACK_12_ERROR = 0x54
    TRACK_13_ERROR = 0x55
    TRACK_23_ERROR = 0x56

##
## Result3
##
class COMMAND_STATE(Enum):
    COMMAND_EXECUTION_END = 0x30
    COMMAND_NOT_POSSIBLE  = 0x32
    COMMAND_EXECUTING     = 0x33 #waiting for card?
    CARD_INSERT_PEND      = 0x34
    DISPENCER_EMPTY       = 0x35
    NO_DISPENSER          = 0x36
    CARD_FULL             = 0x37


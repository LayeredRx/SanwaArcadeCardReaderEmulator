

from CardReaderBase.CardReaderConstants import *
import struct
import threading
import serial
import time

class CardReaderBase:
    """CardReaderBase:
    
    This class is a base class for the Sanwa card reader emulator it implements
    all functions common between different games, but those functions can be
    overloaded in the game specific implementations if needed.
    
    When constructed it will start a thread that loops reading input form the
    serial, and calls the coresponding function as required.
    """
    def __init__(self, serialPort, cardType):
        self.cardType = cardType
        #Variables for Result1 response
        self.SlotDoorState  = CARDSLOT_CLOSED
        self.DispencerState = DISPENCER_FULL
        self.CardPresence   = CARDSLOT_EMPTY
        #End of Result1
        self.Result2 = ERROR_STATE.SUCCESSFUL
        self.Result3 = COMMAND_STATE.COMMAND_EXECUTION_END

        self.comport = serial.Serial(serialPort, baudrate=38400, timeout=0, 
                                     parity=serial.PARITY_EVEN, 
                                     bytesize = serial.EIGHTBITS, 
                                     stopbits=serial.STOPBITS_ONE)
        self.comport.last_byte = time.perf_counter
        self.comport.setRTS(True)
        self.comport = serialPort
        self.currentCard = None
        self.StopRequested = False
        self.emu_thread = threading.Thread(target=self.ControlThread)
        self.card_slot_thread = threading.Thread(target=self.CardSlotThread)
        self.emu_thread.start()
        #self.card_slot_thread.start()
        self.emu_thread.join()
        #self.card_slot_thread.join()
    
    def ControlThread(self):
        """ControlThread()
        This should be a thread that is started in the constructor 
        that will receive the input from the system and drive the state changes
        and command responses"""
        while not self.StopRequested:
            cur_Byte = self.ReadData(1)
            if cur_Byte == PACKET_PREAMBLE: 
                packet_length = self.ReadData(1)
                packet =  self.ReadData(packet_length)
                command_byte = packet[0]
                #verify checksum at end
                #if pass, call command function
                try:
                    self.__getattribute__("Command_%s"%hex(command_byte)[2:].lower())(packet)
                except AttributeError:
                    print("Command %x not known at this time\n\t"%(command_byte), end="")
                    for i in packet:
                        print(hex(i)[2:],end="")
                    print()

    
    def CardSlotThread(self):
        """CardSlotThread()
        This should be a thread that is started in the constructor 
        that will interact as the physical card slot the user manipulates"""
        while not self.StopRequested:
            if self.CardPresence == CARDSLOT_EMPTY and self.SlotDoorState == CARDSLOT_OPEN:
                #check NFC for a new card.
                self.InsertCard()
                pass
            if self.CardPresence == CARDSLOT_OCCUPIED and self.SlotDoorState == CARDSLOT_OPEN:
                #check NFC for card removal
                self.TakeCard()
                pass


    def GenerateCRCbyte(self, packet):
        """GenerateCRCbyte():
        Generate the CRC checksum for the provided data packed and
        return that checksum as a single byte (intger under 255 is fine)
        
        Args:
            packet (list) : a list of integer bytes that compose the packet
        Returns:
            byte : a single byte checksum for the provided packet"""
        CRC = 0
        for i in packet:
            CRC=CRC^i
        return CRC & 0xFF
    
    def WriteACK(self):
        """WriteACK():
        Helper function to quickly write the ACK byte back to the system
        """
        self.comport.write(struct.pack("B", STATUS.ACK))
    
    def WriteData(self, command, packet, result2=ERROR_STATE.SUCCESSFUL, result3=COMMAND_STATE.COMMAND_EXECUTION_END):
        """WriteData():
        Write the provided data back to the system
        
        Args:
            command (integer) : The byte number of the command that is writing the data
            packet  (list)    : The list of bytes the command wishes to send
            result2 (integer) : The byte for result 2 if used by the command
            result3 (integer) : The byte for result 3 if used by the command"""
        packet_bytes = struct.pack('B', PACKET_PREAMBLE)
        packet_bytes = struct.pack('sB', packet_bytes, len(packet) + 6)
        packet_bytes = struct.pack('sB', packet_bytes, command)
        packet_bytes = struct.pack('ss', packet_bytes, packet)
        packet_bytes = struct.pack('sB', packet_bytes, self.SlotDoorState | self.DispencerState | self.CardPresence)
        packet_bytes = struct.pack('sB', packet_bytes, result2)
        packet_bytes = struct.pack('sB', packet_bytes, result3)
        packet_bytes = struct.pack('sB', packet_bytes, END_OF_COMMAND)
        packet_bytes = struct.pack('sB', packet_bytes, self.GenerateCRCbyte(packet_bytes))
        self.comport['ser'].write(packet_bytes)
    
    def ReadData(self, length):
        """ReadData()
        Read some data from the serial port and return it either as a list of 
        integers or as a single integer if only 1 byte is requested
        
        Args:
            length (integer) : The number of bytes that have been requested
        Returns:
            byte/[bytes] : Either a single byte or a list of bytes"""
        if length == 1: #return single byte as integer
            return struct.unpack("B",self.comport['ser'].read(1))[0]
        else: #return a list of bytes as integers
            return [i for i in struct.unpack("%dB"%length,self.comport['ser'].read(length))]
    
    def TakeCard(self):
        """TakeCard()
        Attempt to remove card from the card reader. This is only possible if the card
        reader has ejected the card for the user and there is a card to take.
        
        Returns:
            currentCard : a CartBase object
        """
        if self.SlotDoorState != CARDSLOT_OPEN:
            raise Exception("Unable to take card, reader closed")
        if self.currentCard is None:
            raise Exception("Unable to take card, no card present")
        tempCard = self.currentCard
        self.currentCard = None
        self.cardPresence = CARDSLOT_EMPTY
        return tempCard
        
    def InsertCard(self, card):
        """InsertCard()
        Attempt to insert a card into the card reader. This is only possible if the card
        reader is requesting a card and does not still have a card in the slot.
        
        Args:
            card (CardBase) : A card object for the game in use"""
        if self.SlotDoorState != CARDSLOT_OPEN:
            raise Exception("Wrong time to insert card, reader not ready")
        if self.CardPresence != CARDSLOT_EMPTY:
            raise Exception("Wrong time to insert card, card in reader")
        if self.currentCard is not None:
            raise Exception("Cannot insert card without taking existing card out")
        if not isinstance(card, self.cardType):
            raise Exception("Error trying to insert from an incorrect game")
        self.cardPresence = CARDSLOT_OCCUPIED #might not need to apply these just yet...
        self.SlotDoorState = CARDSLOT_CLOSED #might not need to apply these just yet...
        self.currentCard = card
        

        
    #below are functions that respond to received commands, they can be overloaded in the
    #game support functions as required. or implemented here if they apply to all games.
    def Command_05(self, packet): # ping pong
        self.WriteACK()

    def Command_10(self, packet): #CRWInitialize
        self.WriteACK()
        if self.currentCard is None:
            self.SlotDoorState  = CARDSLOT_OPEN
            self.CardPresence   = CARDSLOT_EMPTY
            self.WriteData(0x10, "")
        else:
            raise Exception("Attemping initial state when card already present")

    def Command_20(self, packet): #CRWReadStatus
        self.WriteACK()
        self.WriteData(0x20, "")

    def Command_30(self, packet): #CRWReadData
        print("Command 30 not implemented", packet)

    def Command_33(self, packet): #CRWReadDataL
        print("Command 33 not implemented", packet)

    def Command_35(self, packet): #CRWLGetData
        print("Command 35 not implemented", packet)

    def Command_40(self, packet): #CRWCancel
        print("Command 40 not implemented", packet)

    def Command_50(self, packet): #CRWLWriteData
        print("Command 50 not implemented", packet)

    def Command_53(self, packet): #CRWWriteDataL
        print("Command 53 not implemented", packet)
        if self.currentCard:
            self.currentCard.setGameData(packet)

    def Command_55(self, packet): #CRWLWriteDataDischargeCard
        print("Command 55 not implemented", packet)

    def Command_5c(self, packet): #CRWLWriteBinaryData
        print("Command 5c not implemented", packet)

    def Command_70(self, packet): # CRWLPrint
        print("Command 70 not implemented", packet)

    def Command_73(self, packet): #CRWLPrint20Lines
        print("Command 73 not implemented", packet)

    def Command_75(self, packet): #CRWLPrintDischargeCard
        print("Command 75 not implemented", packet)

    def Command_78(self, packet): #CRWSetPrint
        self.WriteACK()
        self.WriteData(0x78, "") #no data to write back other than command and status
        #set print head, really a no op 
    
    def Command_7a(self, packet): #CRWRegisterFont
        print("Command 7a not implemented", packet)
    
    def Command_7b(self, packet): #CRWLPrintImage
        print("Command 7b not implemented", packet)
    
    def Command_7c(self, packet): #CRWPrintL
        self.WriteACK()
        self.WriteData(0x7C, "") #no data to write back other than command and status

    def Command_7e(self, packet): #CRWLPrintBarCode
        print("Command 7e not implemented", packet)

    def Command_80(self, packet): #CRWDischargeCard
        self.WriteACK()
        self.SlotDoorState = CARDSLOT_OPEN
        self.CardPresence = CARDSLOT_EMPTY
        if self.currentCard is None:
            self.currentCard = self.cardType()

    def Command_90(self, packet): #CRWLDischargeCardDispenser
        print("Command 90 not implemented", packet)

    def Command_a0(self, packet): #CRWCleaning
        print("Command a0 not implemented", packet)

    def Command_b0(self, packet): #CRWTakenCardDispenser request blank card from dispenser
        self.WriteACK()
        if self.currentCard is None:
            self.SlotDoorState  = CARDSLOT_CLOSED
            self.CardPresence   = CARDSLOT_DISPENCE
            self.WriteData(0xB0, "")
            self.currentCard = self.cardType()
        else:
            raise Exception("Requested card from dispencer when card present")

    def Command_c0(self, packet): #CRWLControlLED
        print("Command c0 not implemented", packet)
    
    def Command_c1(self, packet): #CRWLSetRetry
        print("Command c1 not implemented", packet)

    
    def Command_d0(self, packet): #Actuate shutter "02 07 D0 00 00 00 30 03 E4"
        self.WriteACK()
        if self.currentCard is None:
            self.WriteData(0xD0,b"\x06\x02\x06\xD0\xB8\x30\x33\x03\x6E", result3=COMMAND_EXECUTING)

    def Command_e1(self, packet): #CRWLSetRtcDate
        print("Command e1 not implemented", packet)

    def Command_f0(self, packet): #CRWLGetVersion
        print("Command f0 not implemented", packet)
    
    def Command_f1(self, packet): #CRWLGetRtcDate
        print("Command f1 not implemented", packet)

    def Command_f5(self, packet): #CRWLBattCheck
        print("Command f5 not implemented", packet)
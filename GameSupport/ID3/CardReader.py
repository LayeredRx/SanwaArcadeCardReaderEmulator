
from CardReaderBase.CardReaderBase import CardReaderBase
from GameSupport.ID3.Card import Card

class CardReader(CardReaderBase):
    def __init__(self, serialPort):
        CardReaderBase.__init__(self, serialPort, Card)
